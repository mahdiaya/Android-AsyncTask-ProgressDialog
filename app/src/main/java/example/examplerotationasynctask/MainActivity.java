package example.examplerotationasynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * Proyecto: ExampleRotationAsyncTask
 * Creado por Gotcha on 03-03-2015, malbornozj@gmail.com.
 */
public class MainActivity extends Activity {
    private RotatorProgressDialogTask task;
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                task = new RotatorProgressDialogTask();
                task.startProcess(MainActivity.this);
            }
        });

        if(savedInstanceState!=null){
            if(savedInstanceState.containsKey("task")) {
                Log.d(TAG, "recovering task");
                task = (RotatorProgressDialogTask) savedInstanceState.getSerializable("task");
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(task!=null && !task.isCancelled() && task.getStatus() != AsyncTask.Status.FINISHED) {
            Log.d(TAG, "preserving task");
            outState.putSerializable("task", task);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(task!=null)
            task.stopDialog();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(task!=null)
            task.showDialogIfRunning(this);
    }
}

