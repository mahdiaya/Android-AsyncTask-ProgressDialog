package example.examplerotationasynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

import java.io.Serializable;

/**
 * Proyecto: ExampleRotationAsyncTask
 * Creado por Gotcha on 03-03-2015, malbornozj@gmail.com.
 */
public class RotatorProgressDialogTask extends AsyncTask<Void, Void, Void> implements Serializable {
    private ProgressDialog dialog;
    private final String TAG = "Task";

    public void startProcess(Context context){
        execute();
        showDialogIfRunning(context);
    }

    public void showDialogIfRunning(Context context){
        if(!isCancelled() && getStatus()==Status.RUNNING) {
            startDialog(context);
        }else {
            stopDialog();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.d(TAG, "Canceled");
        stopDialog();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG, "Processing");
    }

    @Override
    protected Void doInBackground(Void... unused) {
        for (int i=0;i<20;i++) {
            if(isCancelled())
                return null;
            SystemClock.sleep(500);
        }
        return(null);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        stopDialog();
        Log.d(TAG, "Finished");
    }

    public void stopDialog() {
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
        dialog = null;
    }

    private void startDialog(Context context) {
        dialog = new ProgressDialog(context);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
            }
        });
        dialog.setMessage("Processing...");
        dialog.show();
    }
}